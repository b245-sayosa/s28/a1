// console.log("S28 Activity")

//adding one into my collection Room
db.rooms.insertOne({
		name: "single",
		accomodation: "2",
		price: 1000,
		description: "A room fit for a small family going on a vacation.",
		roomsAvailable: 5,
		isAvailable: false
	})

db.rooms.insertMany([{
		name: "dounle",
		accomodation: "3",
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: false
	}, {
		
		name: "single",
		accomodation: "4",
		price: 4000,
		description: "A room with queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	
	}])
//finding rooms with name double
db.rooms.find({name: "double"});

//updating queen set roomsAvailable to 0
db.rooms.updateOne({
			name: "queen"
		},{
			$set:{
				roomsAvailable: 0
			}
		})

//deleting rooms with 0 roomsAvail
db.rooms.deleteMany({roomsAvailable: 0})

